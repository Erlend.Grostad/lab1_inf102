package INF102.lab1.triplicate;

import java.util.Hashtable;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        int size = list.size();
        Hashtable<T, Integer> frontier = new Hashtable<>();
        for (int i = 0; i < size; i++) {
            Integer current = frontier.putIfAbsent(list.get(i), 1);
            if (current != null) {
                current++;
                frontier.put(list.get(i), current);
                if (current == 3) {
                    return list.get(i);
                }
            }
        }
        return null;
    }
    
}
